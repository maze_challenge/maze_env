import os
import numpy as np
import pygame
from sys import exit
from maze_env.msg import Pos
red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)
darkBlue = (0,0,128)
white = (255,255,255)
black = (0,0,0)
pink = (255,200,200)
orange = (253, 106, 2)

class Maze():
    def __init__(self, filename, start, goal):
        self.dirpath = os.path.dirname(os.path.abspath(__file__))
        self.maze = np.loadtxt(os.path.split(self.dirpath)[0]+'/resources'+filename, delimiter=',')
        # maze parameters
        self.height = self.maze.shape[0]
        self.width = self.maze.shape[0]
        #Size of canvas
        self.size_win_x = 480*2
        self.size_win_y = 480*2
        #From and To points for route planning
        self.start = start
        self.goal = goal
        #Variables used for graph functions
        self.screen = []
        self.myfont = []
        self.titleFont = []
        self.block_size_x = self.size_win_x / self.width
        self.block_size_y = self.size_win_y / self.height
        self.current_pos = None
        self.num_steps = 0
        #Map Info
        self.num_obstacles = 0
        self.pos_obstacles = []
        # Start game flag
        self.startGame = True
        # First action flag
        self.firstAction = False
        #-----------------------------------

    def init_maze(self):
        self.current_pos = self.start
        pygame.init()
        self.screen = pygame.display.set_mode((self.size_win_x,self.size_win_y))
        self.screen.fill(white)
        pygame.font.init()
        self.myfont = pygame.font.SysFont('Comic Sans MS', 20)
        self.titleFont = pygame.font.SysFont('Comic Sans MS', 32)
        for y in range(self.height):
            for x in range(self.width):
                rect = pygame.Rect(x*self.block_size_x, y*self.block_size_y,
                                    self.block_size_x, self.block_size_y)
                if not self.maze[y][x]:
                    pygame.draw.rect(self.screen, darkBlue, rect, 1)
                    position = Pos()
                    position.x = x
                    position.y = y
                    self.pos_obstacles.append(position)
                else:
                    pygame.draw.rect(self.screen, black, rect, 0)
                if self.start == (y,x):
                    pygame.draw.rect(self.screen, red, rect, 0)
                if self.goal == (y,x):
                    pygame.draw.rect(self.screen, green, rect, 0)
        pygame.display.update()
        self.num_obstacles = len(self.pos_obstacles)
        if self.startGame: self.message_display('Start Game', True)

    # Clear maze
    def clear_maze(self):
        self.screen.fill(white)
        for y in range(self.height):
            for x in range(self.width):
                rect = pygame.Rect(x*self.block_size_x, y*self.block_size_y,
                                    self.block_size_x, self.block_size_y)
                if not self.maze[y][x]:
                    pygame.draw.rect(self.screen, darkBlue, rect, 1)
                    position = Pos()
                    position.x = x
                    position.y = y
                    self.pos_obstacles.append(position)
                else:
                    pygame.draw.rect(self.screen, black, rect, 0)
                if self.start == (y,x):
                    pygame.draw.rect(self.screen, red, rect, 0)
                if self.goal == (y,x):
                    pygame.draw.rect(self.screen, green, rect, 0)
        pygame.display.update()
        self.num_obstacles = len(self.pos_obstacles)

    def give_map_info(self):
        return self.num_obstacles, self.pos_obstacles

    def message_display(self, text, start):
        largeText = pygame.font.Font('freesansbold.ttf',32)
        TextSurf, TextRect = self.text_objects(text, largeText, start)
        TextRect.center = ((self.size_win_x/2),(self.size_win_y/2))
        self.screen.blit(TextSurf, TextRect)
        pygame.display.update()

    # Mehtod to display on screen the number of steps
    def score_display(self):
        self.clear_maze()
        largeText = pygame.font.Font('freesansbold.ttf',26)
        steps = largeText.render("Steps: " + str(self.num_steps), True, blue )
        self.screen.blit(steps, (self.size_win_x - 125, 3))
        pygame.display.update()

    def text_objects(self, text, font, start):
        if start: textSurface = font.render(text, True, green)
        else: textSurface = font.render(text, True, red)
        return textSurface, textSurface.get_rect()

    #time.sleep(2)

    def apply_action(self, action):
        if self.startGame == False and self.firstAction == False:
            self.init_maze()
            self.firstAction = True
        state = ''

        # Update number of steps on screen
        self.num_steps += 1
        self.score_display()

        new_pos = [0,0]
        if action=='up':
            new_pos[0] = self.current_pos[0] - 1 if self.current_pos[0] - 1 else 0
            new_pos[1] = self.current_pos[1]
        elif action=='right':
            new_pos[0] = self.current_pos[0]
            new_pos[1] = self.current_pos[1] + 1 if self.current_pos[1] + 1 else 0
        elif action=='down':
            new_pos[0] = self.current_pos[0] + 1 if self.current_pos[0] + 1 else 0
            new_pos[1] = self.current_pos[1]
        elif action=='left':
            new_pos[0] = self.current_pos[0]
            new_pos[1] = self.current_pos[1] - 1 if self.current_pos[1] - 1 else 0
        else:
            state = 'alive'

        #print('curr pos: ' + str(self.current_pos))
        #print('new pos: ' + str(new_pos))
        if not self.maze[new_pos[0]][new_pos[1]]:
            rect = pygame.Rect(self.current_pos[1]*self.block_size_x,
                            self.current_pos[0]*self.block_size_y,
                            self.block_size_x, self.block_size_y)
            pygame.draw.rect(self.screen, white, rect, 0)
            pygame.display.update()
            pygame.draw.rect(self.screen, darkBlue, rect, 1)
            pygame.display.update()
            self.current_pos = new_pos
            rect = pygame.Rect(self.current_pos[1]*self.block_size_x,
                                self.current_pos[0]*self.block_size_y,
                                self.block_size_x, self.block_size_y)
            pygame.draw.rect(self.screen, red, rect, 0)
            pygame.display.update()
            state = 'alive'
        else:
            state = 'collision'
            rect = pygame.Rect(self.current_pos[1]*self.block_size_x,
                            self.current_pos[0]*self.block_size_y,
                            self.block_size_x, self.block_size_y)
            pygame.draw.rect(self.screen, red, rect, 0)
            pygame.display.update()
        rect = pygame.Rect(self.goal[1]*self.block_size_x,
                            self.goal[0]*self.block_size_y,
                            self.block_size_x, self.block_size_y)
        pygame.draw.rect(self.screen, green, rect, 0)
        if new_pos == self.goal: self.message_display('Game Over (' + str(self.num_steps) + ' Steps)', False)

        return self.current_pos, state

    def closeWindow(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                pygame.display.quit()
                exit()
                return True
            return False
