#! /usr/bin/env python3
import rospy
from std_msgs.msg import String
from maze_env.msg import Pos
from maze_env.srv import MapInfo, MapInfoResponse
import pygame
import threading
import time
import numpy as np
import os
from maze import Maze


class Env_Maze():

	def __init__(self):                                          #Maze parameters
		self.filename = '/Bigmaze.txt'
		#self.filename = '/Bigmaze.txt'
		self.start = [0,1]
		self.goal = [28,29]
		#self.goal = [58,59]
		self.msg_pos = Pos()
		self.maze = None
		self.num_obstacles = 0
		self.pos_obstacles = 0
		self.positionPub = None
		self.start_srv = None
		self.goal_srv = None
		print("--------------------------------------")
		print("-------------BIENVENIDOS--------------")
		print("--------------------------------------")
		print("-----------A LA ETAPA I DEL-----------")
		print("--------------------------------------")
		print("----------AI CHALLENGE 201920---------")
		print("--------------------------------------")
		print("Por favor correr el agente para comenzar el juego")

	def actionCallback(self, data):                              #This is executed each time an action is published
		self.maze.startGame = False
		rospy.loginfo("Action: %s", data.data)                   #Print agent's action
		new_agent_pos, state = self.maze.apply_action(data.data) #Agent's position is showed in maze
		rospy.loginfo('state: ' + state)
		self.msg_pos.x = new_agent_pos[1]                        #Updates agent's position in x
		self.msg_pos.y = new_agent_pos[0]                        #Updates agent's position in y
		self.positionPub.publish(self.msg_pos)                   #Publish Agent's position

	def srvMapInfoCallback(self, data):                          #Service of map information
		rospy.loginfo('Name: {}'.format(data.name))              #Print map name
		self.start_srv = Pos()                                   #Construction of service start position message
		self.start_srv.x = self.start[0]
		self.start_srv.y = self.start[1]
		self.goal_srv = Pos()                                    #Construction of service goal position message
		self.goal_srv.x = self.goal[0]
		self.goal_srv.y = self.goal[1]
		return MapInfoResponse(self.num_obstacles, self.pos_obstacles, self.start_srv, self.goal_srv) #return service message

	def main(self):
		self.maze = Maze(self.filename, self.start, self.goal)                  #Maze is created
		self.maze.init_maze()                                                   #Initialize maze
		self.num_obstacles, self.pos_obstacles = self.maze.give_map_info()      #Initialize obstacle information
		rospy.init_node('maze_env')                                             #Initialize node
		self.positionPub = rospy.Publisher('/position', Pos, queue_size=10)     #Create publisher of agent's actual postion
		rospy.Subscriber("/action", String, self.actionCallback)                #Create subscriber to agent's action
		map_srv = rospy.Service('/map_info', MapInfo, self.	srvMapInfoCallback) #Initialize service of map information
		threading.Thread(target=self.threadExitGame()).start()                  #Starting Thread for killing the game
		rospy.spin()

	def threadExitGame(self):                                                   #Thread for killing the game
		while True:
			exit = self.maze.closeWindow()
			if exit: sys.exit()
			time.sleep(500E-3)

if __name__ == "__main__":
	env_maze = Env_Maze()
	env_maze.main()
